﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.MQConsumer
{
    public class RabbitMQPromocodeNotifyConsumerHostedService : BackgroundService
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<RabbitMQPromocodeNotifyConsumerHostedService> _logger;
        private IConnection _rabbitConnection;
        private IModel _rabbitChannel;
        private string queueName = "notifyPromocodes";

        public RabbitMQPromocodeNotifyConsumerHostedService(ILogger<RabbitMQPromocodeNotifyConsumerHostedService> logger,
                                                           IServiceProvider services)
        {
            _logger = logger;
            _services = services;

            InitRabbit();
        }

        private void InitRabbit()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost",
                Port = 5672
            };
            _rabbitConnection = factory.CreateConnection();
            _rabbitChannel = _rabbitConnection.CreateModel();

            
            var exchangeName = "promoCodes";
            _rabbitChannel.ExchangeDeclare(exchangeName, ExchangeType.Direct, true, false);

            var queueDeclareResponce = _rabbitChannel.QueueDeclare(queueName, true, false, false);
            
        }

        private async void ProcessMessage(string message)
        {
            using (var scope = _services.CreateScope())
            {
                var scopedProcessingService =
                    scope.ServiceProvider
                        .GetRequiredService<IGivePromocodeNotifyMessageProcessingService>();

                await scopedProcessingService.ProcessMessageWork(message);
            }

            _logger.LogInformation($"Queue {queueName}, processed message: {message}");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var consumer = new EventingBasicConsumer(_rabbitChannel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body.ToArray());
                _logger.LogInformation($"Received from {queueName} message: {message}");
                ProcessMessage(message);
            };
            _rabbitChannel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);

            return Task.CompletedTask;
        }

    }
}

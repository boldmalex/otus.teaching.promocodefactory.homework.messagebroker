﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public interface IGivePromocodeNotifyMessageProcessingService
    {
        Task ProcessMessageWork(string message);
    }
}

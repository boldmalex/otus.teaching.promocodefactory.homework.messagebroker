﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class GivePromocodeNotifyMessageProcessingService : IGivePromocodeNotifyMessageProcessingService
    {
        private readonly IRepository<Employee> _employeeRepository;


        public GivePromocodeNotifyMessageProcessingService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }


        public async Task ProcessMessageWork(string message)
        {
            var id = new Guid(message);
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee != null)
            {
                employee.AppliedPromocodesCount++;

                await _employeeRepository.UpdateAsync(employee);
            }
        }
    }
}

﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class RabbitMQMessageBroker : IGivingPromoCodeToCustomerGateway, IAdministrationGateway
	{

		static private IConnection GetRabbitConnection()
		{
			ConnectionFactory factory = new ConnectionFactory
			{
				UserName = "guest",
				Password = "guest",
				HostName = "localhost",
				Port = 5672
			};
			IConnection conn = factory.CreateConnection();
			return conn;
		}


		public Task GivePromoCodeToCustomer(PromoCode promoCode)
		{
			using (var connection = GetRabbitConnection())
			using (var channel = connection.CreateModel())
			{
				var routingKey = "givePromocodes";

				var exchangeName = "promoCodes";
				channel.ExchangeDeclare(exchangeName, ExchangeType.Direct, true, false);

				var queueName = "givePromocodes";
				var queueDeclareResponce = channel.QueueDeclare(queueName, true, false, false);

				channel.QueueBind(queueName, exchangeName, routingKey);

				var dto = new GivePromoCodeToCustomerDto()
				{
					PartnerId = promoCode.Partner.Id,
					BeginDate = promoCode.BeginDate.ToShortDateString(),
					EndDate = promoCode.EndDate.ToShortDateString(),
					PreferenceId = promoCode.PreferenceId,
					PromoCode = promoCode.Code,
					ServiceInfo = promoCode.ServiceInfo,
					PartnerManagerId = promoCode.PartnerManagerId
				};

				string message = Newtonsoft.Json.JsonConvert.SerializeObject(dto);
				var body = Encoding.UTF8.GetBytes(message);

				channel.BasicPublish(exchange: exchangeName,
									 routingKey: routingKey,
									 basicProperties: null,
									 body: body);
			}

			return Task.CompletedTask;
		}


		public Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
			using (var connection = GetRabbitConnection())
			using (var channel = connection.CreateModel())
			{
				var routingKey = "notifyPromocodes";

				var exchangeName = "promoCodes";
				channel.ExchangeDeclare(exchangeName, ExchangeType.Direct, true, false);

				var queueName = "notifyPromocodes";
				var queueDeclareResponce = channel.QueueDeclare(queueName, true, false, false);

				channel.QueueBind(queueName, exchangeName, routingKey);

				var body = Encoding.UTF8.GetBytes(partnerManagerId.ToString());

				channel.BasicPublish(exchange: exchangeName,
									 routingKey: routingKey,
									 basicProperties: null,
									 body: body);
			}

			return Task.CompletedTask;
		}
    }
}

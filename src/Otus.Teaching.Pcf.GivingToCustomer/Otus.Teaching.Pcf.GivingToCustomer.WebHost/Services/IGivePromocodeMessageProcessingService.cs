﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public interface IGivePromocodeMessageProcessingService
    {
        Task ProcessMessageWork(string message);
    }
}
